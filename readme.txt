Scripts is a module that allows you to run cmd line, php scripts or both in different ways. You can set a php script that will run on every page load, cron runs or by accessing a menu callback for that script

Example:

In this example we're going to write a script that will return a list of files and folders with all hidden files filterd by size. To do this in linux you run this command "ls -l -a -s"

Now we're going write a small script to do that and run on every page load in our site

1. Go to /admin/build/scripts in your drupal site
2. Click on the add tab
3. Complete the form like this
	a. Name: Write the name of the script, let's say "list"
	b. Description : Script description 
	c. Body: The body of our script. Here you can write PHP code or cmd line or both, in this example we need a cmd code "ls" with three arguments "-l -a -s" so what I write here is: ls %1 %2 %3 
			%1 %2 %3 are the arguments that I'm going to pass to the ls command "we will see how to pass those arguments below"
	d. Run on init: If you want this script to run on every page load in your site then tick this option, this is what we need in our example 
	e. Run on cron: If you want this script to run with the cron job in your site then tick this option
	f. Cron run time every?: every how many minutes do you want your script to run "this will work only if you chose the run on cron option:
	g. Arguments: what are the arguments you wana pass to the script when it's run through the init or cron separate each argument with "|"
			in this example we want to pass these three arguments "-l -a -s" so this is wut we're going to write here "-l|-a|-s"
			in step c we wrote (%1 %2 %3) and this is what's going to happen here, the %1 will be replaced with the first argument -l, %2 will be replaced with -a and %3 with -s
	h. Maximum execution time: This is the time limit set for this script to finish running. Make sure it's enough for your script to run. If your script took more than this time php is gonna return a fatal error.

Now click save, a new script with the name "list" will be added to your scripts. This script is going to run on ever page load, you can even run it manullay, every script in the list has a form where you can put the arguments that you want to pass to the script and then hit run now button. You can also run the script through a menu callback from the url, for example to run this example using menu callback go to this url "/scripts/run/list/-l/-a/-s"

Each script has a menu callback with this structure "scripts/run/~script name~/arg1/arg2/arg3/....../argn"

4. The module also provides an API for other modules to extend it with pre-defined scripts. The svn script provided out of the box is an example. Modules need only implmenet hook_scripts. Check out the existing hook_scripts inside the scripts module.

We're looking forward to get all kinds of script plugins, scripts for backup, for svn and for anything else that comes to mind.

Enjoy!